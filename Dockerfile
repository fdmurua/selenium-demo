FROM debian
USER root

ENV HOME=/data/apps
ENV AEM_BASE=/data/apps/aem
ENV AEM_HOME=/data/apps/aem/publish
ENV JAVA_HOME=/opt/jdk


#Install needed tools (wget, gnupg, apt-transport-https, rpm)
RUN apt-get update
RUN apt-get install -my wget gnupg
RUN apt-get update && apt-get install -y apt-transport-https
RUN apt-get install -y rpm

#Install Xvfb (inspired in https://github.com/kfatehi/docker-chrome-xvfb) and configure
RUN apt-get install -y -q unzip xvfb
ADD xvfb_init.sh /etc/init.d/xvfb
RUN chmod a+x /etc/init.d/xvfb
ADD xvfb-daemon-run.sh /usr/bin/xvfb-daemon-run
RUN chmod a+x /usr/bin/xvfb-daemon-run

#Install firefox 52.9.0
RUN apt update
RUN apt install -y firefox-esr

#Install google-chrome-stable
RUN echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list
RUN wget https://dl-ssl.google.com/linux/linux_signing_key.pub
RUN apt-key add linux_signing_key.pub
RUN apt-get update
RUN apt-get install -y google-chrome-stable

#Install Java
RUN mkdir ${JAVA_HOME}
COPY jdk-8u181-linux-x64.tar.gz ${JAVA_HOME}
WORKDIR ${JAVA_HOME}
RUN tar -zxf jdk-8u181-linux-x64.tar.gz -C ${JAVA_HOME}
RUN update-alternatives --install /usr/bin/java java ${JAVA_HOME}/jdk1.8.0_181/bin/java 100
RUN update-alternatives --install /usr/bin/javac javac ${JAVA_HOME}/jdk1.8.0_181/bin/javac 100
RUN update-alternatives --display java
RUN update-alternatives --display javac

ENV JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/

#Install Jenkins
RUN wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | apt-key add -
RUN sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
RUN apt-get update
RUN apt-get install jenkins -y --force-yes
RUN update-rc.d jenkins defaults
ADD jenkins /var/lib/jenkins
RUN chown -R jenkins:jenkins /var/lib/jenkins

#CMD service jenkins start

#Install Maven
RUN export 
RUN apt-get install -y maven

#Install Git
RUN apt-get install -y git

#Install mozilla geckodriver
WORKDIR ${HOME}
RUN wget https://github.com/mozilla/geckodriver/releases/download/v0.18.0/geckodriver-v0.18.0-linux64.tar.gz
RUN tar -xvzf geckodriver*
RUN chmod +x ${HOME}/geckodriver
RUN export PATH=$PATH:${HOME}/geckodriver

#Install chrome driver
RUN wget https://chromedriver.storage.googleapis.com/2.40/chromedriver_linux64.zip
RUN unzip chromedriver_linux64.zip
RUN chmod +x chromedriver

#Install apache
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y apache2
RUN apt-get install -y apache2-doc 
RUN apt-get install -y apache2-utils
RUN sed -i -e 's/KeepAlive On/KeepAlive Off/g' /etc/apache2/apache2.conf

#Install aem-publish
RUN mkdir -p ${AEM_BASE}/publish
WORKDIR ${AEM_BASE}
ADD aemPublish /etc/init.d/
RUN chmod +x /etc/init.d/aemPublish
RUN useradd aem
WORKDIR ${AEM_HOME}
COPY aem-publish-4503.jar ${AEM_HOME}/crx-quickstart.jar
RUN java -jar crx-quickstart.jar -unpack
ADD licence.properties ${AEM_HOME}/license.properties
RUN chown -R aem:aem ${AEM_HOME}
RUN update-rc.d aemPublish defaults
#as the better suggestion from adobe guides
RUN sed -i -e 's/CQ_PORT=4502/CQ_PORT=4503/g' ${AEM_HOME}/crx-quickstart/bin/start
RUN sed -i -e 's/CQ_RUNMODE=\x27author\x27/CQ_RUNMODE=\x27publish\x27/g' ${AEM_HOME}/crx-quickstart/bin/start

#Install dispatcher
WORKDIR ${HOME}
RUN mkdir /var/www/html/publish
RUN mkdir /etc/apache2/vhosts
RUN mkdir /etc/apache2/conf
RUN wget https://www.adobeaemcloud.com/content/companies/public/adobe/dispatcher/dispatcher/_jcr_content/top/download_8/file.res/dispatcher-apache2.4-linux-x86-64-4.2.3.tar.gz
RUN tar -xvzf dispatcher-apache2.4-linux-x86-64-4.2.3.tar.gz
RUN cp dispatcher-apache2.4-4.2.3.so /usr/lib/apache2/modules/
ADD dispatcher.any /etc/apache2/conf/dispatcher.any
ADD apache2.conf /etc/apache2/apache2.conf
ADD qa-publish.conf /etc/apache2/vhosts/qa-publish.conf

#Install firefox in order to update it to 60
RUN apt update
RUN apt install -y firefox-esr

#EXPOSE 4503
EXPOSE 8080
#EXPOSE 80