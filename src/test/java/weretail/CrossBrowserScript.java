package weretail;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxDriverLogLevel;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.Select;
import org.springframework.util.FileCopyUtils;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class CrossBrowserScript {

	WebDriver driver;
	
	/** The base url. */
    protected String baseUrl = "http://localhost/content/we-retail/us/en.html";
    
    protected String browser;
    
    private StringBuffer verificationErrors = new StringBuffer();

	/**
	 * Setup the driver
	 * @param browser
	 * @throws Exception
	 */
	@BeforeTest
	@Parameters("browser")
	@BeforeClass(alwaysRun = true)
	public void setup(String browser) throws Exception{
		this.browser = browser;

		if(browser.equalsIgnoreCase("firefox")){
			
			System.setProperty("webdriver.gecko.driver", "/data/apps/geckodriver");
			
			FirefoxOptions options = new FirefoxOptions();
			options.setLogLevel(FirefoxDriverLogLevel.DEBUG);
			options.setBinary("/usr/bin/firefox");
//			options.setHeadless(true);
//			options.setLegacy(false);
			options.setCapability("marionette", true);
			driver = new FirefoxDriver(options);
			
		} else if(browser.equalsIgnoreCase("chrome")){
			
			System.setProperty("webdriver.chrome.driver", "/data/apps/chromedriver");
			
			ChromeOptions options = new ChromeOptions();
			options.setBinary("/usr/bin/google-chrome-stable");
			options.addArguments("start-maximized"); // open Browser in maximized mode
			options.addArguments("--disable-extensions"); // disabling extensions
			options.addArguments("--disable-gpu"); // applicable to windows os only
			options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
			options.addArguments("--no-sandbox");
//			options.setExperimentalOption("useAutomationExtension", true);
//			options.setHeadless(true);
			
			driver = new ChromeDriver(options);
			
		} else {
			//If no browser passed throw exception
			throw new Exception("Browser is not correct");
		}
		
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
	}
	
	/**@AfterTest
	public void finish() throws Exception{
		driver.quit();
	}**/
	
	private void takeScreenshot( int i ) throws IOException {
		File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileCopyUtils.copy(screenshotFile, new File("screenshotFileTest"+ i + "-" + browser + ".png"));
	}
	
	@Test
	public void testCheckoutProcess() throws IOException {
		
		driver.get(baseUrl);
		driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Women'])[3]/following::a[1]")).click();
	    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='off'])[3]/following::a[1]")).click();
	    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='XXL'])[1]/following::button[1]")).click();
	    driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[2]/a")).click();
	    driver.findElement(By.xpath("/html/body/div[1]/div/div/div[2]/div/div[5]/a")).click();
	    driver.findElement(By.id("form-text-1460947019")).click();
	    driver.findElement(By.id("form-text-1460947019")).clear();
	    driver.findElement(By.id("form-text-1460947019")).sendKeys("franco");
	    driver.findElement(By.id("form-text-246733039")).clear();
	    driver.findElement(By.id("form-text-246733039")).sendKeys("murua");
	    driver.findElement(By.id("form-text-251335646")).clear();
	    driver.findElement(By.id("form-text-251335646")).sendKeys("general lopez 3681");
	    driver.findElement(By.id("form-text-489791559")).clear();
	    driver.findElement(By.id("form-text-489791559")).sendKeys("santa fe");
	    new Select(driver.findElement(By.id("form-options-1203914010"))).selectByVisibleText("Argentina");
	    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Gift & Promotional Codes'])[1]/preceding::div[5]")).click();
	    driver.findElement(By.id("form-text-68713227")).click();
	    driver.findElement(By.id("form-text-68713227")).clear();
	    driver.findElement(By.id("form-text-68713227")).sendKeys("4444333322221111");
	    driver.findElement(By.id("form-text-1651705086")).clear();
	    driver.findElement(By.id("form-text-1651705086")).sendKeys("01/23");
	    driver.findElement(By.id("form-text-935029260")).clear();
	    driver.findElement(By.id("form-text-935029260")).sendKeys("123");
	    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Gift & Promotional Codes'])[1]/following::button[1]")).click();
	    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Gift & Promotional Codes'])[1]/following::button[1]")).click();
	    assertEquals(driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='THANK YOU !'])[1]/following::h4[1]")).getText(), "Your order is being processed.");
	    takeScreenshot(1);
	    
	    
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() throws Exception {
		if(!browser.equalsIgnoreCase("firefox")) {
			driver.quit();
		    String verificationErrorString = verificationErrors.toString();
		    if (!"".equals(verificationErrorString)) {
		      fail(verificationErrorString);
		    }
		}
	  }
	
}
